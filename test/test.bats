#!/usr/bin/env bats
#
# Test NPM Publish
#

set -e

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/npm-publish"}
  docker build -t ${DOCKER_IMAGE} .
}

@test "Publish package to NPM" {

  suffix=$(date -u "+%Y%m%d%H%M%S")
  npm version 2.0.0-$suffix --no-git-tag-version --prefix test/

  # execute tests
  run docker run \
    -e NPM_TOKEN="$NPM_TOKEN" \
    -e FOLDER="test" \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}
  
  [[ "${status}" == "0" ]]
  [[ "${output}" =~ "published successfully." ]]

}

@test "Publishing an existing version should fail" {

  # execute tests
  run docker run \
    -e NPM_TOKEN="$NPM_TOKEN" \
    -e FOLDER="test" \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "1" ]]
  [[ "${output}" =~ "Failed to publish package" ]]

}
